package com.gai.dailytasks.service;

import java.util.Map;

public interface DailyTaskService {
	
	public Map<String,Object> getPopulators();

}
