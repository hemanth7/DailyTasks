package com.gai.dailytasks.service;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gai.dailytasks.dao.DailyTasksDao;
@Service("dailyTaskService")
public class DailyTaskServiceImpl implements DailyTaskService{

	@Autowired
	private DailyTasksDao dailyTasksDao;
	@Override
	public Map<String, Object> getPopulators() {
		
		return dailyTasksDao.getPopulators();
	}

}
