package com.gai.dailytasks.entities;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="PROJECT")
public class Project extends AbstractObject {

	
	@Override
	public String toString() {
		return "Project [id=" + getId() + ", name=" + getName() + "]";
	}
}
