package com.gai.dailytasks.entities;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="TASK_USER")
public class TaskUser extends AbstractObject {

	@Override
	public String toString() {
		return "User [id=" + getId() + ", name=" + getName() + "]";
	}
	
}
