package com.gai.dailytasks.entities;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


import com.gai.dailytasks.util.Status;

@Entity
@Table(name = "DAILY_TASKS")
public class DailyTaskEntitiy {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Column(name = "ID")
	private Integer id;
	
	@Column(name="JIRAID")
	private String jiraId;
	
	@Column(name="TEAM")
	private String team;
	
	@ManyToOne(cascade=CascadeType.ALL,fetch=FetchType.LAZY)
	@JoinColumn(name="CATEGORYID")
	private Category category;
	
	@ManyToOne(cascade=CascadeType.ALL,fetch=FetchType.LAZY)
	@JoinColumn(name="PROJECTID")
	private Project project;
	
	@Column(name="DESCRIPTION")
	private String description;
	
	@Column(name="RELEASE")
	private String release;
	
	@Column(name="PLANNEDSTARTDATE")
	private Date plannedStartDate;
	
	@Column(name="PLANNEDENDDATE")
	private Date plannedEndDate;
	
	@Column(name="ACTUALSTARTDATE")
	private Date actualStartDate;
	
	@Column(name="ACTUALENDDATE")
	private Date actualEndDate;

	@Column(name="ESTIMATEDHOURS")
	private int estimatedHours;

	@Column(name = "STATUS")
	@Enumerated(EnumType.STRING)
	private Status status;
	
	@Column(name="PERCENTAGECOMPELETION")
	private int percentageCompeletion;
	
	@Column(name="DEPLOYEMENTDATE")
	private Date deployementDate;
	
	@Column(name="comments")
	private String comments;
	
	@Column(name="USERID")
	private TaskUser user;
	
	@Column(name="DATE")
	private Date date;
	
	@Column(name="PRIORITY")
	private String priority;
	
	@Column(name="ACTIVITY")
	private String activity;
	
	@Column(name="PLANNEDHOURS")
	private int plannedHours;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getJiraId() {
		return jiraId;
	}

	public void setJiraId(String jiraId) {
		this.jiraId = jiraId;
	}

	public String getTeam() {
		return team;
	}

	public void setTeam(String team) {
		this.team = team;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategoryId(Category category) {
		this.category = category;
	}

	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getRelease() {
		return release;
	}

	public void setRelease(String release) {
		this.release = release;
	}

	public Date getPlannedStartDate() {
		return plannedStartDate;
	}

	public void setPlannedStartDate(Date plannedStartDate) {
		this.plannedStartDate = plannedStartDate;
	}

	public Date getPlannedEndDate() {
		return plannedEndDate;
	}

	public void setPlannedEndDate(Date plannedEndDate) {
		this.plannedEndDate = plannedEndDate;
	}

	public Date getActualStartDate() {
		return actualStartDate;
	}

	public void setActualStartDate(Date actualStartDate) {
		this.actualStartDate = actualStartDate;
	}

	public Date getActualEndDate() {
		return actualEndDate;
	}

	public void setActualEndDate(Date actualEndDate) {
		this.actualEndDate = actualEndDate;
	}

	public int getEstimatedHours() {
		return estimatedHours;
	}

	public void setEstimatedHours(int estimatedHours) {
		this.estimatedHours = estimatedHours;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public int getPercentageCompeletion() {
		return percentageCompeletion;
	}

	public void setPercentageCompeletion(int percentageCompeletion) {
		this.percentageCompeletion = percentageCompeletion;
	}

	public Date getDeployementDate() {
		return deployementDate;
	}

	public void setDeployementDate(Date deployementDate) {
		this.deployementDate = deployementDate;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public TaskUser getUser() {
		return user;
	}

	public void setUserId(TaskUser user) {
		this.user = user;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getPriority() {
		return priority;
	}

	public void setPriority(String priority) {
		this.priority = priority;
	}

	public String getActivity() {
		return activity;
	}

	public void setActivity(String activity) {
		this.activity = activity;
	}

	public int getPlannedHours() {
		return plannedHours;
	}

	public void setPlannedHours(int plannedHours) {
		this.plannedHours = plannedHours;
	}

	@Override
	public String toString() {
		return "DailyTaskEntitiy [id=" + id + ", jiraId=" + jiraId + ", team=" + team + ", category=" + category
				+ ", project=" + project + ", description=" + description + ", release=" + release
				+ ", plannedStartDate=" + plannedStartDate + ", plannedEndDate=" + plannedEndDate + ", actualStartDate="
				+ actualStartDate + ", actualEndDate=" + actualEndDate + ", estimatedHours=" + estimatedHours
				+ ", status=" + status + ", percentageCompeletion=" + percentageCompeletion + ", deployementDate="
				+ deployementDate + ", comments=" + comments + ", user=" + user + ", date=" + date + ", priority="
				+ priority + ", activity=" + activity + ", plannedHours=" + plannedHours + "]";
	}

	
	
	
}
