package com.gai.dailytasks.entities;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="CATEGORY")
public class Category extends AbstractObject{

	@Override
	public String toString() {
		return "Category [id=" + getId() + ", name=" + getName() + "]";
	}

}
