package com.gai.dailytasks.modal;

import java.util.Date;



public class DailyTasksDto {
	
	private int id;
	
	private String jiraId;

	private String team;

	private int categoryId;

	private int projectId;

	private String description;

	private String release;

	private Date plannedStartDate;

	private Date plannedEndDate;

	private Date actualStartDate;

	private Date actualEndDate;

	private int estimatedHours;

	private String status;

	private int percentageCompeletion;

	private Date deployementDate;

	private String comments;

	private int userId;

	private Date date;

	private String priority;

	private String activity;

	private int plannedHours;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getJiraId() {
		return jiraId;
	}

	public void setJiraId(String jiraId) {
		this.jiraId = jiraId;
	}

	public String getTeam() {
		return team;
	}

	public void setTeam(String team) {
		this.team = team;
	}

	public int getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}

	public int getProjectId() {
		return projectId;
	}

	public void setProjectId(int projectId) {
		this.projectId = projectId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getRelease() {
		return release;
	}

	public void setRelease(String release) {
		this.release = release;
	}

	public Date getPlannedStartDate() {
		return plannedStartDate;
	}

	public void setPlannedStartDate(Date plannedStartDate) {
		this.plannedStartDate = plannedStartDate;
	}

	public Date getPlannedEndDate() {
		return plannedEndDate;
	}

	public void setPlannedEndDate(Date plannedEndDate) {
		this.plannedEndDate = plannedEndDate;
	}

	public Date getActualStartDate() {
		return actualStartDate;
	}

	public void setActualStartDate(Date actualStartDate) {
		this.actualStartDate = actualStartDate;
	}

	public Date getActualEndDate() {
		return actualEndDate;
	}

	public void setActualEndDate(Date actualEndDate) {
		this.actualEndDate = actualEndDate;
	}

	public int getEstimatedHours() {
		return estimatedHours;
	}

	public void setEstimatedHours(int estimatedHours) {
		this.estimatedHours = estimatedHours;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public int getPercentageCompeletion() {
		return percentageCompeletion;
	}

	public void setPercentageCompeletion(int percentageCompeletion) {
		this.percentageCompeletion = percentageCompeletion;
	}

	public Date getDeployementDate() {
		return deployementDate;
	}

	public void setDeployementDate(Date deployementDate) {
		this.deployementDate = deployementDate;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getPriority() {
		return priority;
	}

	public void setPriority(String priority) {
		this.priority = priority;
	}

	public String getActivity() {
		return activity;
	}

	public void setActivity(String activity) {
		this.activity = activity;
	}

	public int getPlannedHours() {
		return plannedHours;
	}

	public void setPlannedHours(int plannedHours) {
		this.plannedHours = plannedHours;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((activity == null) ? 0 : activity.hashCode());
		result = prime * result + ((actualEndDate == null) ? 0 : actualEndDate.hashCode());
		result = prime * result + ((actualStartDate == null) ? 0 : actualStartDate.hashCode());
		result = prime * result + categoryId;
		result = prime * result + ((comments == null) ? 0 : comments.hashCode());
		result = prime * result + ((date == null) ? 0 : date.hashCode());
		result = prime * result + ((deployementDate == null) ? 0 : deployementDate.hashCode());
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + estimatedHours;
		result = prime * result + id;
		result = prime * result + ((jiraId == null) ? 0 : jiraId.hashCode());
		result = prime * result + percentageCompeletion;
		result = prime * result + ((plannedEndDate == null) ? 0 : plannedEndDate.hashCode());
		result = prime * result + plannedHours;
		result = prime * result + ((plannedStartDate == null) ? 0 : plannedStartDate.hashCode());
		result = prime * result + ((priority == null) ? 0 : priority.hashCode());
		result = prime * result + projectId;
		result = prime * result + ((release == null) ? 0 : release.hashCode());
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		result = prime * result + ((team == null) ? 0 : team.hashCode());
		result = prime * result + userId;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DailyTasksDto other = (DailyTasksDto) obj;
		if (activity == null) {
			if (other.activity != null)
				return false;
		} else if (!activity.equals(other.activity))
			return false;
		if (actualEndDate == null) {
			if (other.actualEndDate != null)
				return false;
		} else if (!actualEndDate.equals(other.actualEndDate))
			return false;
		if (actualStartDate == null) {
			if (other.actualStartDate != null)
				return false;
		} else if (!actualStartDate.equals(other.actualStartDate))
			return false;
		if (categoryId != other.categoryId)
			return false;
		if (comments == null) {
			if (other.comments != null)
				return false;
		} else if (!comments.equals(other.comments))
			return false;
		if (date == null) {
			if (other.date != null)
				return false;
		} else if (!date.equals(other.date))
			return false;
		if (deployementDate == null) {
			if (other.deployementDate != null)
				return false;
		} else if (!deployementDate.equals(other.deployementDate))
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (estimatedHours != other.estimatedHours)
			return false;
		if (id != other.id)
			return false;
		if (jiraId == null) {
			if (other.jiraId != null)
				return false;
		} else if (!jiraId.equals(other.jiraId))
			return false;
		if (percentageCompeletion != other.percentageCompeletion)
			return false;
		if (plannedEndDate == null) {
			if (other.plannedEndDate != null)
				return false;
		} else if (!plannedEndDate.equals(other.plannedEndDate))
			return false;
		if (plannedHours != other.plannedHours)
			return false;
		if (plannedStartDate == null) {
			if (other.plannedStartDate != null)
				return false;
		} else if (!plannedStartDate.equals(other.plannedStartDate))
			return false;
		if (priority == null) {
			if (other.priority != null)
				return false;
		} else if (!priority.equals(other.priority))
			return false;
		if (projectId != other.projectId)
			return false;
		if (release == null) {
			if (other.release != null)
				return false;
		} else if (!release.equals(other.release))
			return false;
		if (status == null) {
			if (other.status != null)
				return false;
		} else if (!status.equals(other.status))
			return false;
		if (team == null) {
			if (other.team != null)
				return false;
		} else if (!team.equals(other.team))
			return false;
		if (userId != other.userId)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "DailyTasksDto [id=" + id + ", jiraId=" + jiraId + ", team=" + team + ", categoryId=" + categoryId
				+ ", projectId=" + projectId + ", description=" + description + ", release=" + release
				+ ", plannedStartDate=" + plannedStartDate + ", plannedEndDate=" + plannedEndDate + ", actualStartDate="
				+ actualStartDate + ", actualEndDate=" + actualEndDate + ", estimatedHours=" + estimatedHours
				+ ", status=" + status + ", percentageCompeletion=" + percentageCompeletion + ", deployementDate="
				+ deployementDate + ", comments=" + comments + ", userId=" + userId + ", date=" + date + ", priority="
				+ priority + ", activity=" + activity + ", plannedHours=" + plannedHours + "]";
	}

}
