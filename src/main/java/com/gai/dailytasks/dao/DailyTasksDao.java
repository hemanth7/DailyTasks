package com.gai.dailytasks.dao;

import java.util.Map;

public interface DailyTasksDao {
	
	public Map<String,Object> getPopulators();

}
