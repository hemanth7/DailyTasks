package com.gai.dailytasks.dao;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.NavigableSet;
import java.util.TreeSet;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import com.gai.dailytasks.entities.Category;
import com.gai.dailytasks.entities.DailyTaskEntitiy;
import com.gai.dailytasks.entities.Project;
import com.gai.dailytasks.entities.TaskUser;


@Repository("dailyTasksDao")
public class DailyTasksDaoImpl implements DailyTasksDao {
	
	@PersistenceContext
	private EntityManager entityManager;
	
	
	public Map<String,Object> getPopulators(){
		Map<String,Object> map=new HashMap<>();
		List<Category> cat=getProperties("select c from Category c",Category.class,null);
		map.put("category", cat);
		List<Project> pro=getProperties("select p from Project p",Project.class,null);
		map.put("project", pro);
		List<TaskUser> user=getProperties("select p from Project p",TaskUser.class,null);
		map.put("user", user);
		return map;
		
	}
	
	private<T> List<T> getProperties(String sql,Class<T> t,Map<String,Object> parameters){
		TypedQuery<T> query=entityManager.createQuery(sql,t);
		if(!parameters.isEmpty()) {
			for(Entry<String,Object> entry:parameters.entrySet()) {
				query.setParameter(entry.getKey(), entry.getValue());
			}
		}
		return query.getResultList();
	}
	
	
	public List<DailyTaskEntitiy> getDailyTasks(Map<String,Object> filters){
		String sql="select d from DailyTaskEntitiy d ";
		if(!filters.isEmpty()) {
			sql=sql.concat(sqlQueryWithFilters(filters));
		}	
		sql=sql.concat(sqlQueryWithFilters(filters));
		return getProperties(sql,DailyTaskEntitiy.class,filters);
	}
	@SuppressWarnings("unchecked")
	private String sqlQueryWithFilters(Map<String,Object> filters) {
		StringBuffer sql=new StringBuffer(" ");
		boolean userflag=filters.containsKey("user");
		if(userflag) {
			sql.append(" inner join d.user u where ");
			Object users=filters.get("user");
			if(users instanceof List) {
				sql.append(" u.id IN :user ");
			}else if(users instanceof Integer) {
				sql.append(" u.id=:user ");
			}
		}
		if(filters.containsKey("date")){
			if(!userflag) {
				sql.append(" where ");
			}else {
				sql.append(" AND ");
			}
			Object date=filters.get("date");
			if(date instanceof List) {
				Map<String,String>dateMap=getDates((List<String>)date);
				filters.remove("date");
				filters.putAll(dateMap);
				sql.append(" d.date BETWEEN TO_DATE(:from ,'yyyy/mm/dd') AND TO_DATE(:to ,'yyyy/mm/dd') ");
			}else if(date instanceof String) {
				sql.append(" TO_CHAR(d.date,'yyyy/mm/dd') =:date ");
			}
		}
		return sql.toString();
	}
	private Map<String,String> getDates(List<String>dates){
		Map<String,String> map=new HashMap<>();
		NavigableSet<Date> set=new TreeSet<>();
		SimpleDateFormat sf=new SimpleDateFormat("yyyy/MM/dd");
		for(String dateString:dates) {
			try {
				set.add(sf.parse(dateString));
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}if(!set.isEmpty()) {
			map.put("from",sf.format(set.first()));
			map.put("to",sf.format(set.last()));
		}
		return map;
	}

}
