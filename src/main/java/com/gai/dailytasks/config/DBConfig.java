package com.gai.dailytasks.config;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;


import javax.sql.DataSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.instrument.classloading.InstrumentationLoadTimeWeaver;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaDialect;
import org.springframework.transaction.annotation.EnableTransactionManagement;


import oracle.jdbc.pool.OracleDataSource;
import oracle.ucp.jdbc.PoolDataSource;
import oracle.ucp.jdbc.PoolDataSourceFactory;
@Configuration 
@EnableTransactionManagement
@PropertySource("classpath:database.properties")
public class DBConfig {

    private static final String PERSISTENCE_UNIT_NAME="LOCAL_PERSISTENCE";
	
	  public DataSource dataSource() throws SQLException
	    {
	        final PoolDataSource dataSource = PoolDataSourceFactory.getPoolDataSource();
	        dataSource.setConnectionPoolName(ConnectionPoolNameProvider.getPoolName(PERSISTENCE_UNIT_NAME));
	        dataSource.setURL("jdbc:oracle:thin:@//localhost:1521/orcl.global.trafigura.com");
	        dataSource.setUser("system");
	        dataSource.setPassword("root");
	        dataSource.setConnectionFactoryClassName(OracleDataSource.class.getName());
	        dataSource.setMinPoolSize(10);
	        dataSource.setMaxPoolSize(100);
	        dataSource.setInactiveConnectionTimeout(100);
	        dataSource.setValidateConnectionOnBorrow(true);
	        dataSource.setFastConnectionFailoverEnabled(false);
	        dataSource.setConnectionProperty("sql.explain_plan","false"); // This property is used by DAOUtil
	        dataSource.setDataSourceName("dailyTaskDatasource");
	        return dataSource;
	    }
	
	
 @Bean
 public LocalContainerEntityManagerFactoryBean geEntityManagerFactoryBean() {
   try {
  	 final LocalContainerEntityManagerFactoryBean bean = new LocalContainerEntityManagerFactoryBean();
       bean.setLoadTimeWeaver(new InstrumentationLoadTimeWeaver());
       bean.setDataSource(dataSource());
       bean.setJpaDialect(new HibernateJpaDialect());
       final Map<String, String> jpaPropertyMap = new HashMap<String, String>();
       jpaPropertyMap.put("hibernate.show_sql", "true");
       jpaPropertyMap.put("hibernate.hbm2ddl.auto", "update");
       bean.setJpaPropertyMap(jpaPropertyMap);
       bean.setPersistenceUnitName(PERSISTENCE_UNIT_NAME);
       return bean;
   }catch (Exception e) {
		e.printStackTrace();
		return null;
	}	  
 }

 @Bean
 public JpaTransactionManager geJpaTransactionManager() {
    JpaTransactionManager transactionManager = new JpaTransactionManager();
    transactionManager.setEntityManagerFactory(geEntityManagerFactoryBean().getObject());
    return transactionManager;
 }
    
    
    
}
