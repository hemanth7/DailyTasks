package com.gai.dailytasks.config;
public final class ConnectionPoolNameProvider
{
    private ConnectionPoolNameProvider()
    {
    }

    public static String getPoolName(String persistenceUnitName)
    {
        return persistenceUnitName + "Pool-" + System.currentTimeMillis();
    }
}
