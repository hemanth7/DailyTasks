package com.gai.dailytasks.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.gai.dailytasks.modal.DailyTasksDto;
import com.gai.dailytasks.service.DailyTaskService;

@Controller
@RequestMapping("/")
public class DailyTaskController {
	
	@Autowired
	private DailyTaskService dailyTaskService;
	
	@RequestMapping(method=RequestMethod.POST,produces=MediaType.APPLICATION_JSON_VALUE,value="getDailyTasks")
	@ResponseBody
	public List<DailyTasksDto> getDailyTasks(@RequestBody Map<String,Object> filters) {
		return null;
	}
	@RequestMapping(method=RequestMethod.POST,produces=MediaType.APPLICATION_JSON_VALUE,value="addTask")
	@ResponseBody
	public String addOrEditTasks(@RequestBody Map<String,Object> dailyTasks) {
		return null;	
	}
	@RequestMapping(method=RequestMethod.POST,produces=MediaType.APPLICATION_JSON_VALUE,value="deleteTask")
	@ResponseBody
	public String deleteTask(@RequestBody Map<String,Object> dailyTasks) {
		return null;
	}
	@RequestMapping(method=RequestMethod.POST,produces=MediaType.APPLICATION_JSON_VALUE,value="getPopulators")
	@ResponseBody
	public Map<String,Object> getPopulators(){
		return dailyTaskService.getPopulators();
	}

}
