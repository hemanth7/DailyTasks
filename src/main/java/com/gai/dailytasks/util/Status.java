package com.gai.dailytasks.util;

public enum Status {
	WILL_START("will start"),IN_PROGRESS("In Progress");
	
	private String description;
	
	Status(String description){
		this.description=description;
	}
	
	public String getDescription() {
		return this.description;
	}
	
	public Status getEnum(String description) {
		for(Status status:Status.values()) {
			if(status.getDescription().equals(description)){
				return status;
			}
		}
		return null;
	}
	
}
